       IDENTIFICATION DIVISION. 
       PROGRAM-ID. WRITE-GRADE1.
       AUTHOR. SUWIGAK.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE  ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           05 STU-ID   PIC   X(8).
           05 MIDTERM-SCORE  PIC   9(2)V9(2).
           05 FINAL-SCORE PIC   9(2)V9(2).
           05 PROJECT-SCORE  PIC   9(2)V9(2).
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE 
           MOVE "39003001" TO STU-ID
           MOVE "34.05" TO MIDTERM-SCORE  
           MOVE "25.25" TO FINAL-SCORE 
           MOVE "10.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL             
      

           MOVE "39003002" TO STU-ID
           MOVE "60.05" TO MIDTERM-SCORE  
           MOVE "30.25" TO FINAL-SCORE 
           MOVE "20.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL             
       

           MOVE "39003003" TO STU-ID
           MOVE "45.05" TO MIDTERM-SCORE  
           MOVE "10.25" TO FINAL-SCORE 
           MOVE "15.8" TO PROJECT-SCORE 
           WRITE SCORE-DETAIL             
           CLOSE SCORE-FILE    
           GOBACK
           .

